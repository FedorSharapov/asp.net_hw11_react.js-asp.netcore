namespace IdentityServer.Models
{
    public class UserRegister
    {
        public string UserName{ get; set; }
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}