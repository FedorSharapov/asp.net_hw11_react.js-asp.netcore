﻿using IdentityServer.Models;

namespace IdentityServer.Data
{
    public static class FakeUsers
    {
        public static List<User> Users = new List<User>
        {
            new User{UserName = "guest", Password= "guest" },
            new User{UserName = "admin", Password= "admin" }
        };
    }
}
