var builder = WebApplication.CreateBuilder(args);

const string _myAllowSpecificOrigins = "ReactApp";

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: _myAllowSpecificOrigins, 
        policy =>
        {
            policy.WithOrigins("http://localhost:3000")
            .WithMethods("POST") 
            .WithHeaders("Content-Type");
        });
});
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(_myAllowSpecificOrigins);
app.MapControllers();

app.Run();
