using IdentityServer.Models;
using Microsoft.AspNetCore.Mvc;
using IdentityServer.Data;

namespace IdentityServer.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        public AuthController() { }

        [HttpPost("[action]")]
        public IActionResult Login([FromBody] User user)
        {
            var result = FakeUsers.Users.FirstOrDefault(x => x.UserName == user.UserName && x.Password == user.Password);

            if (result != null)
                return Ok(user.UserName);
            
            return BadRequest("Wrong login or password.");
        }

        [HttpPost("[action]")]
        public IActionResult Register([FromBody] UserRegister newUser)
        {
            if (newUser.UserName.Length < 2)
                return BadRequest("Login must be at least 2 characters!");

            if (newUser.Password.Length < 6)
                return BadRequest("Password must be at least 6 characters!");

            if (newUser.Password != newUser.ConfirmPassword)
                return BadRequest("Passwords do not match!");

            var user = FakeUsers.Users.FirstOrDefault(x => x.UserName == newUser.UserName);
            if (user != null)
                return BadRequest("User with this name already exists!");

            FakeUsers.Users.Add(new User
            {
                UserName = newUser.UserName,
                Password = newUser.Password
            });

            return Ok(newUser.UserName);
        }
    }
}