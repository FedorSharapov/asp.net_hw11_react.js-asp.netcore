import {useSelector} from 'react-redux'

function WelcomePage(props){
    const isAuthorized = useSelector(state => state.login.isAuthorized);

    if(!isAuthorized)
        return props.navigationUnauthorized;

    const userName = useSelector(state => state.login.userName);

    return <div className="page welcome">
        <h1>Hello, {userName}!</h1>
        <p>This is JavaScript React homework. OTUS ASP.NET course.</p>
    </div>
}

WelcomePage.defaultProps = {navigationUnauthorized:{}};
export default WelcomePage;