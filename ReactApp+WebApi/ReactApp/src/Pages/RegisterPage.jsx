import { useState } from 'react'
import {useDispatch} from 'react-redux'
import {loggedIn} from '../Storage/LoginReducer'
import {useNavigate} from 'react-router-dom'

import Register from '../Api/RegisterApi'

import RegisterForm from '../Components/Forms/RegisterForm'


function RegisterPage(props){
    const [stateLogin, setStateLogin] = useState({isError:false,errMsg:''});

    const dispatch = useDispatch();
    const navigate = useNavigate();

    async function registerHandler(){
        const userNameValue = document.getElementById('reg_login1').value;
        const passwordValue = document.getElementById('reg_password1').value;
        const confirmpasswordValue = document.getElementById('reg_password2').value;

        try{
            const result = await Register(userNameValue,passwordValue,confirmpasswordValue);
            if(result.status == 200){
                dispatch(loggedIn({userName:result.body}));
                navigate(props.returnUrl);

                setStateLogin({isError: false, errMsg:' '});
            }
            else{
                setStateLogin({isError: true, errMsg: result.body});
            }
        } catch(err){
            alert(`Error: ${err.message}`)
        }
    }

    return <RegisterForm onRegisterClick={registerHandler} isError={stateLogin.isError} errMsg={stateLogin.errMsg} />
}

RegisterPage.defaultProps = {returnUrl:'/'};
export default RegisterPage;