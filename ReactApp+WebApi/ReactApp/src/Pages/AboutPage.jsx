import { Outlet } from "react-router-dom";
import {NavLink} from "react-router-dom";

export function AboutPage(props){
    return <div className="page">
       <br/>
       This is JavaScript React homework.

       <div className="about-navbar">
        <nav >
            <NavLink to={props.paths.ABOUT_COURSE}>Course</NavLink>
            <NavLink to={props.paths.ABOUT_PERIOD}>Period</NavLink>
        </nav>
        <main>
            <Outlet />
        </main>

        </div>
    </div>
}

export function AboutCourse(){
    return <p>C# ASP.NET Core разработчик</p>
}

export function AboutPeriod(){
    return <p>30 November 2022 — 4 June 2023</p>
}


AboutPage.defaultProps = {paths:{}};