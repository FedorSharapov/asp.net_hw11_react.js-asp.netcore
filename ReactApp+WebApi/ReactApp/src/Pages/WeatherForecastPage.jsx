import {useState, useEffect} from 'react'

import GetWeatherForecast from '../Api/WeatherForecastApi';
import WeatherForecastTable from '../Components/WeatherForecastTable';


function RegisterPage(){
    const [state, setState] = useState({forecasts: [],loading: true});

    useEffect(() => {
        const fetchData = async () => {
            const data = await GetWeatherForecast();
            setState({forecasts: data, loading: false});
         }
         fetchData();
      }, []);

    return <div>
        <h1 id="tabelLabel" >Weather forecast</h1>
        <p>This component demonstrates fetching data from the server.</p>
        {
            state.loading ?
            <p><em>Loading...</em></p>:
            <WeatherForecastTable forecasts={state.forecasts} />
        }
    </div>
}

export default RegisterPage;