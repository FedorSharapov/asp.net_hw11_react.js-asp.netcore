import {useDispatch, useSelector} from 'react-redux'
import { decreased, increased } from '../Storage/CounterReducer';

import Counter from "../Components/Counter/Counter";


function HomePage(){
    const value = useSelector(state => state.counter.value);
    const dispatch = useDispatch();

    return <div className="page">
       <Counter 
            value={value} 
            decreaseClick={() => dispatch(decreased())}
            increaseClick={() => dispatch(increased())}
        />
    </div>
}

export default HomePage;