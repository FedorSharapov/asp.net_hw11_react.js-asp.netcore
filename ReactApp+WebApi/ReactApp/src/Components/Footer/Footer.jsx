import './Footer.css';

function Footer({year,text}) {
  return (
    <footer className="App-footer">
      (C) {year} &nbsp; {text}
    </footer>
  );
}

export default Footer;