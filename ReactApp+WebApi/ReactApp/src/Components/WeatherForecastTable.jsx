import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses }  from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { styled } from '@mui/material/styles';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: '#5b88c4ea',
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: '#cfe1f971',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function WeatherForecastTable(props) {  
  return (
    <div style={{width: '500px',position:'relative', margin: 'auto'}}>
      <Table sx={{border:'1px solid gray'}} aria-label="simple table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">Date</StyledTableCell>
            <StyledTableCell align="center">Temp.&nbsp;(C)</StyledTableCell>
            <StyledTableCell align="center">Temp.&nbsp;(F)</StyledTableCell>
            <StyledTableCell align="center">Summary</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.forecasts.map((forecast) => (
            <StyledTableRow
              key={forecast.date}
              sx={{ '&:last-child td, &:last-child th': { border:'0'} }}
            >
              <StyledTableCell align="center">{(new Date(forecast.date)).toDateString()}</StyledTableCell>
              <StyledTableCell align="center">{forecast.temperatureC}</StyledTableCell>
              <StyledTableCell align="center">{forecast.temperatureF}</StyledTableCell>
              <StyledTableCell align="center">{forecast.summary}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
      </div>
  );
}

WeatherForecastTable.defaultProps = {forecasts:[]};
export default WeatherForecastTable;