import './LogoutForm.css';

function LogoutForm(props){
    return <div >
        <br/>
        <form className='form-logout'>
            <span className='username'>Name: {props.userName}</span>
            <div className='button'>
                <input type='button' id='logout1' value='Logout' onClick={props.onLogoutClick}/>
            </div>
        </form>
    </div>
}

LogoutForm.defaultProps = {Login:'',onLogoutClick:{}}
export default LogoutForm;