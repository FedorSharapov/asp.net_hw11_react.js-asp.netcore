import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { Alert } from '@mui/material';

function RegisterForm(props){
        return <form className="register-form">
        <br/>
        <Grid container
            direction="column"
            justifyContent="flex-start"
            alignItems="center"
            spacing={3}
            border={'-moz-initial'}
        >
            <Grid item xs={6}>
                <TextField  id="reg_login1" label="Login" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <TextField id="reg_password1" label="Password" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <TextField id="reg_password2" label="Confirm password" variant="outlined" size="small"/>
            </Grid>
            <Grid item xs={6}>
                <Button variant="outlined" onClick={props.onRegisterClick}>Register</Button>
            </Grid>
            <Grid item xs={6}>
            {
                props.isError ?
                <Alert severity="error">{props.errMsg}</Alert>:
                <></>
            }
            </Grid>
        </Grid>
    </form>
}

RegisterForm.defaultProps = {onRegisterClick:{},isError:false, errMsg:''}
export default RegisterForm;