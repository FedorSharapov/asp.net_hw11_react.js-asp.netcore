async function Register(userName, password,confirmPassword){
  const response = await fetch('http://localhost:5000/api/Auth/Register',{
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
    body: JSON.stringify({userName,password,confirmPassword})
  });

  const result = {status:500, body:''}
  result.status = response.status;
  result.body = await response.text();

  console.log(response.url, result.status);

  return result;
}

Register.defaultProps = {userName:'',password:'',confirmPassword:''}
export default Register;