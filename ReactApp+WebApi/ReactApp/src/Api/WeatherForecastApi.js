async function GetWeatherForecast(){
    const response = await fetch('http://localhost:5000/api/WeatherForecast/Get');
    const data = await response.json();
    console.log(response.url, response.status);
  
    return data;
}
  
export default GetWeatherForecast;