// еще один способ создание Reducer

import { createSlice } from "@reduxjs/toolkit";

const counterSlice =createSlice({
    name:'counter',
    // application's state
    initialState:{
        value: 0
    },
    // Reducer with actions
    reducers:{
        decreased(state){
            state.value -= 1
        },
        increased(state){
            state.value += 1
        }
    }
})

export default counterSlice.reducer;
export const {increased, decreased} = counterSlice.actions;